<section class="login-modal">
    <div class="wrapper">
        <div class="remodal rounded" data-remodal-id="modal">
            <button data-remodal-action="close" class="remodal-close"></button>
            <h4 class="display-4 maincol demo-custom mb-3">Login</h4>
            <p class="maincol">Molimo vas unesite sledeće podatke da bi ste se ulogovali:</p>
            <input class="form_deo form-control mt-3 maincol" type="email" placeholder="Vaš E-mail">
            <input class="form_deo form-control mt-3 maincol" type="password" placeholder="Vaša lozinka">

            <a href="" class="text-center d-block mt-2 text-primary">Zaboravili ste lozinku?</a>
            <button class="btn btn-danger btn-lg mt-3">Ulogujte se</button>
        </div>
    </div>
</section>