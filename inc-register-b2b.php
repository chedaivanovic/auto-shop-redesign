<section class="register-b2b-modal searchincluder">
    <div class="wrapper">
        <div class="remodal rounded" data-remodal-id="modal2">
            <button data-remodal-action="close" class="remodal-close"></button>
            <h4 class="display-4 maincol mb-3">Registrujte se B2B</h4>
            <p>Ako imate svoj biznis i želite da se povežete sa nama, molimo vas da unesete sledeće podatke</p>
            <input class="form_deo form-control mt-3" type="email" placeholder="E-mail vaše firme">
            <input class="form_deo form-control mt-3" type="name" placeholder="Naziv vaše firme">
            <input class="form_deo form-control mt-3" type="phone" placeholder="Broj telefona vaše firme">
            <input class="form_deo form-control mt-3" type="password" placeholder="Lozinka">
            <div class="form-check mt-3">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Slažem se sa uslovima korišćenja</label>
            </div>
            <div class="form-check mt-3">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Garantujem da su unešeni podaci tačni</label>
            </div>

            <button class="btn btn-danger btn-lg mt-3">Registrujte se za B2B</button>

        </div>
    </div>
</section>