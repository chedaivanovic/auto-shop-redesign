<?php include("inc-header.php") ?>

<!-- Hero Slider -->
<section class="hero">
    <ul id="lightSlider">
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-beograd-heroslide3.jpg" class="w-100 d-none d-sm-block" alt="">
            <img class="d-block w-100 d-sm-none" src="images/hyundai-kia-rezervni-delovi-beograd-heroslide3r.jpg" class="w-100" alt="">
            <div class="herotext text-center">
                <h4 class="mb-2">Delovi za vaš KIA ili Hyundai automobil?</h4>
                <p>Na pravom ste mestu! Mi smo Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi, suscipit.</p>
            </div>
        </li>
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-beograd-heroslide1.jpg" class="w-100 d-none d-sm-block" alt="">
            <img class="d-block w-100 d-sm-none" src="images/hyundai-kia-rezervni-delovi-beograd-heroslide1r.jpg" class="w-100" alt="">
            <div class="herotext text-center">
                <h4 class="mb-2">Već XX godina smo na tržištu</h4>
                <p>I možemo se pohvaliti da smo sigurno najbolji u svom poslu na ovom području, a tome u prilog ide i informacija da smo samo za ovu godinu...</p>
            </div>
        </li>
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-beograd-heroslide2.jpg" class="w-100 d-none d-sm-block" alt="">
            <img class="d-block w-100 d-sm-none" src="images/hyundai-kia-rezervni-delovi-beograd-heroslide2r.jpg" class="w-100" alt="">
            <div class="herotext text-center">
                <h4 class="mb-2">Kupujete delove za svoj lični automobil</h4>
                <p>za vas smo pripremili specijalne ponude koje možete pogledati na ovom linku:</p>
                <a href="" class="btn btn-danger btn-lg mt-2">POGLEDAJ</a>
            </div>
        </li>
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-beograd-heroslide4.jpg" class="w-100 d-none d-sm-block" alt="">
            <img class="d-block w-100 d-sm-none" src="images/hyundai-kia-rezervni-delovi-beograd-heroslide4r.jpg" class="w-100" alt="">
            <div class="herotext text-center">
                <h4 class="mb-2">Ako ste naš Profesionalni kupac delova</h4>
                <p>za vas smo pripremili specijalne ponude koje možete pogledati na ovom linku:</p>
                <a href="" class="btn btn-danger btn-lg mt-2">POGLEDAJ</a>
            </div>
        </li>
    </ul>
</section>

<section class="firsttext py-2 py-sm-3 py-md-5">
    <div class="wrapper">
        <h2 class="display-4 text-center maincol pb-3">Hyundai i Kia delovi</h2>
        <p class="text-center">Hyundai | Kia | Auto delovi. Specijalizovana prodaja rezervnih delova za automobile Hyundai i Kia. Svi auto-delovi proizvedeni su u Južnoj Koreji. Za svaki model automobila možete poručiti originalni rezervni deo, rezervni deo koji je proizveo proizvođač koji proizvodi delove za prvu ugradnju i zamenski rezervni deo proizveden od strane renomiranih korejskih proizvođača. Bilo da rezervni deo kupujete od nas ili na nekom drugom mestu, obavezno proverite cenu koju plaćate, za isti novac uvek birajte deo boljeg kvaliteta.</p>
        <p class="text-center font-weight-bold">Pozovite i proverite cenu za deo koji vam je potreban.</p>
        <p class="text-center font-weight-bold">Pozovite i proverite kvalitet dela koji vam je potreban.</p>
        <p class="text-center font-weight-bold lastp">Na sve deleve dajemo garanciju od godinu dana (osim za potrošni materijal).</p>
    </div>
</section>
<section class="ctacards pb-5">
    <div class="wrapper">
        <h4 class="display-4 text-white text-center py-5">
            Registrujte se kod nas
        </h4>
        <div class="row">
            <div class="col-12 col-md-6 mb-4 pb-md-0">
                <div class="card bg-white">
                    <img src="images/hyundai-kia-rezervni-delovi-beograd-card1.jpg" alt="">

                    <div class="card-body">
                        <h4 class="card-title maincol">B2B prodaja auto delova</h4>
                        <p class="card-text">Ukoliko ste pravno lice i želite da naručujete auto-delove od nas, molimo vas da koristite naš B-B portal.</p>

                        <a href="" data-remodal-target="modal2" class="btn btn-danger mt-3">B2B Registracija?</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="card bg-white">
                    <img src="images/hyundai-kia-rezervni-delovi-beograd-card2.jpg" alt="">

                    <div class="card-body">
                        <h4 class="card-title maincol">Registracija za fizička lica</h4>
                        <p class="card-text">Ukoliko ste fizičko lice i želite da dobijate od nas obaveštenja o specijalnim ponudama molimo vas da se registrujete.</p>

                        <a href="" data-remodal-target="modal3" class="btn btn-danger mt-3">Registracija?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="included-search">
    <?php include("inc-search.php") ?>
</section>

<section class="sales" style="margin-top:-50px;">
    <ul id="lightSlider2">
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-sale1.jpg" class="w-100 d-none d-sm-block" alt="">
            <img src="images/hyundai-kia-rezervni-delovi-sale1r.jpg" class="w-100 d-block d-sm-none" alt="">
            <div class="herotext2 text-left position-absolute">
                <div class="wrapper py-2 py-sm-5 px-2">
                    <h4 class="display-2 pb-1 pb-sm-4">
                        Akcija
                    </h4>
                    <h4 class="text-uppercase pb-1 pb-sm-3">Kočioni sistem za Hyundai Elantra</h4>
                    <p class="pb-1 pb-sm-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus optio amet laudantium in ea, officiis eaque corrupti quas, nostrum quasi est nihil? Qui numquam architecto harum dicta repudiandae necessitatibus quos.</p>
                    <a href="" class="btn btn-danger btn-lg mt-2" style="width:100%; max-width:350px;">POGLEDAJ</a>
                </div>
            </div>
        </li>
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-sale2.jpg" class="w-100 d-none d-sm-block" alt="">
            <img src="images/hyundai-kia-rezervni-delovi-sale2r.jpg" class="w-100 d-block d-sm-none" alt="">
            <div class="herotext2 text-left position-absolute">
                <div class="wrapper py-2 py-sm-5 px-2">
                    <h4 class="display-2 pb-1 pb-sm-4">
                        Akcija
                    </h4>
                    <h4 class="text-uppercase pb-1 pb-sm-3">Delovi motora za Hyundai Tucson l</h4>
                    <p class="pb-1 pb-sm-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus optio amet laudantium in ea, officiis eaque corrupti quas, nostrum quasi est nihil? Qui numquam architecto harum dicta repudiandae necessitatibus quos.</p>
                    <a href="" class="btn btn-danger btn-lg mt-2" style="width:100%; max-width:350px;">POGLEDAJ</a>
                </div>
            </div>
        </li>
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-sale3.jpg" class="w-100 d-none d-sm-block" alt="">
            <img src="images/hyundai-kia-rezervni-delovi-sale3r.jpg" class="w-100 d-block d-sm-none" alt="">
            <div class="herotext2 text-left position-absolute">
                <div class="wrapper py-2 py-sm-5 px-2">
                    <h4 class="display-2 pb-1 pb-sm-4">
                        Akcija
                    </h4>
                    <h4 class="text-uppercase pb-1 pb-sm-3">Set kvačila za KIA Canada vozila</h4>
                    <p class="pb-1 pb-sm-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus optio amet laudantium in ea, officiis eaque corrupti quas, nostrum quasi est nihil? Qui numquam architecto harum dicta repudiandae necessitatibus quos.</p>
                    <a href="" class="btn btn-danger btn-lg mt-2" style="width:100%; max-width:350px;">POGLEDAJ</a>
                </div>
            </div>
        </li>
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-sale4.jpg" class="w-100 d-none d-sm-block" alt="">
            <img src="images/hyundai-kia-rezervni-delovi-sale4r.jpg" class="w-100 d-block d-sm-none" alt="">
            <div class="herotext2 text-left position-absolute">
                <div class="wrapper py-2 py-sm-5 px-2">
                    <h4 class="display-2 pb-1 pb-sm-4">
                        Akcija
                    </h4>
                    <h4 class="text-uppercase pb-1 pb-sm-3">Lanac u reduktoru za KIA automobile</h4>
                    <p class="pb-1 pb-sm-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus optio amet laudantium in ea, officiis eaque corrupti quas, nostrum quasi est nihil? Qui numquam architecto harum dicta repudiandae necessitatibus quos.</p>
                    <a href="" class="btn btn-danger btn-lg mt-2" style="width:100%; max-width:350px;">POGLEDAJ</a>
                </div>
            </div>
        </li>
        <li class="position-relative">
            <img src="images/hyundai-kia-rezervni-delovi-sale5.jpg" class="w-100 d-none d-sm-block" alt="">
            <img src="images/hyundai-kia-rezervni-delovi-sale5r.jpg" class="w-100 d-block d-sm-none" alt="">
            <div class="herotext2 text-left position-absolute">
                <div class="wrapper py-2 py-sm-5 px-2">
                    <h4 class="display-2 pb-1 pb-sm-4">
                        Akcija
                    </h4>
                    <h4 class="text-uppercase pb-1 pb-sm-3"><span class="d-none d-sm-inline">Veliki izbor</span> rezervni<span class="d-none d-sm-inline">h</span> delov<span class="d-none d-sm-inline">a</span><span class="d-inline d-sm-none">i</span> za vaše KIA i Hyundai automobile<span class="d-none d-sm-inline"> sada na popustu</h4>
                    <p class="pb-1 pb-sm-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus optio amet laudantium in ea, officiis eaque corrupti quas, nostrum quasi est nihil? Qui numquam architecto harum dicta repudiandae necessitatibus quos.</p>
                    <a href="" class="btn btn-danger btn-lg mt-2" style="width:100%; max-width:350px;">POGLEDAJ</a>
                </div>
            </div>
        </li>
    </ul>
</section>

<section class="veleprodaja pb-5 text-center">
    <div class="wrapper">
        <h4 class="display-4 maincol mb-3">Veleprodaja</h4>
        <p class="pb-3">Ukoliko ste servis, prodavnica ili majstor posebno naglasite kada nazovete da bismo vam odobrili rabat prilikom kupovine. Ukoliko se registrujete imate priliku da direktno poručujete robu sa našeg lagera.</p>
        <a href="" class="veleprodaja-button btn btn-danger btn-lg mt-2" style="width:280px;"><span class=""><img src="images/hyundai-kia-rezervni-delovi-beograd-shopping-cart.svg" alt=""></span> E - Shop</a>
    </div>

</section>

<?php include("inc-footer.php") ?>