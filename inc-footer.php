<footer>
    <section class="footer-categories bg-secondary py-5">
        <div class="wrapper">
            <div class="row text-center text-sm-left">
                <col-12 class="col-sm-6 col-md-3">
                    <a href="" class="text-white d-block">Senzori</a>
                    <a href="" class="text-white d-block">Vešanje</a>
                    <a href="" class="text-white d-block">Pumpe</a>
                    <a href="" class="text-white d-block">Delovi za veliki servis</a>
                    <a href="" class="text-white d-block">Delovi za mali servis</a>
                    <a href="" class="text-white d-block">Kočioni sistemi</a>
                </col-12>
                <col-12 class="col-sm-6 col-md-3">
                    <a href="" class="text-white d-block">Creva</a>
                    <a href="" class="text-white d-block">Karoserijski delovi</a>
                    <a href="" class="text-white d-block">Svetlosna grupa</a>
                    <a href="" class="text-white d-block">Limarija</a>
                    <a href="" class="text-white d-block">Pogon točkova</a>
                    <a href="" class="text-white d-block">Filteri</a>
                </col-12>
                <col-12 class="col-sm-6 col-md-3">
                    <a href="" class="text-white d-block">Trap – delovi trapa</a>
                    <a href="" class="text-white d-block">Sistem hlađenja</a>
                    <a href="" class="text-white d-block">Sajle</a>
                    <a href="" class="text-white d-block">Prenosni sistem</a>
                    <a href="" class="text-white d-block">Ležajevi</a>
                    <a href="" class="text-white d-block">Sistem paljenja</a>
                </col-12>
                <col-12 class="col-sm-6 col-md-3">
                    <a href="" class="text-white d-block">Delovi motora</a>
                    <a href="" class="text-white d-block">Registracija vozila</a>
                    <a href="" class="text-white d-block">Rentiranje vozila</a>
                    <a href="" class="text-white d-block">Rent a car Beograd</a>
                    <a href="" class="text-white d-block">Iznajmljivanje kombija</a>
                    <a href="" class="text-white d-block">Polovni viljuškari</a>
                    <a href="" class="text-white d-block">Delta rent a car Beograd</a>
                </col-12>
            </div>
        </div>
    </section>
    <section class="mainfooter">
        <div class="wrapper">
            <div class="row text-white">
                <a href="https://webdizajn-beograd.com/" class="studio77 col-12 col-md-5 text-white text-center text-md-left"> Web Dizajn | Seo optimizacija | Izrada sajtova | Studio77</a>
                <div class="col-12 col-md-2 text-center"><a class="d-block fbfooter" href="https://www.facebook.com/kribzon/" target="_blank"><img src="images/hyundai-kia-rezervni-delovi-beograd-facebook.svg" alt=""></a></div>
                <p class="text white col-12 col-md-5 text-center text-md-right">
                    &copy; 2019. Copyright by Kribzon d.o.o. | Hyundai Kia delovi</p>
            </div>
        </div>
    </section>
</footer>
<script src="js/popper.min.js"></script>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/javascript.js"></script>
<script src="js/lightslider.min.js"></script>
<script src="js/remodal.js"></script>

<?php include("inc-login.php") ?>
<?php include("inc-register-user.php") ?>
<?php include("inc-register-b2b.php") ?>
<?php include("inc-search-modal.php") ?>

</body>

</html>