<!DOCTYPE html>
<html lang="sr">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Rezervni delovi za Hyundai i Kia | Beograd</title>

    <!-- Code resources -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Quicksand:300,400,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="css/lightslider.min.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/remodal.css">
    <link rel="stylesheet" href="css/remodal-default-theme.css">
    <script src="js/jquery-3.4.1.min.js"></script>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png" />
    <link rel="manifest" href="images/favicon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

    <!-- OGP -->
    <meta property="og:title" content="Kribzon - Delovi za Hyundai i KIA automobile, Beograd">
    <meta property="og:site_name" content="Hyundai KIA originalni delovi Beograd">
    <meta property="og:url" content="https://www.hyundaikiadelovi.com/">
    <meta property="og:description" content="Kribzon - Originalni Delovi za Hyundai i KIA automobile za profesionalne (B2B) i individualne kupce.">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://www.hyundaikiadelovi.com/images/kribzon-logo.png">


</head>

<body>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "AutoPartsStore",
            "name": "Kribzon",
            "image": "https://www.hyundaikiadelovi.com/images/kribzon-logo.png",
            "@id": "",
            "url": "https://www.hyundaikiadelovi.com",
            "telephone": "+381668828283",
            "priceRange": "$$",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Gandijeva 152",
                "addressLocality": "Belgrade",
                "postalCode": "11000",
                "addressCountry": "RS"
            },
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": 44.7971303,
                "longitude": 20.3899236
            },
            "openingHoursSpecification": [{
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday"
                ],
                "opens": "08:00",
                "closes": "20:00"
            }, {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": "Saturday",
                "opens": "08:00",
                "closes": "16:00"
            }],
            "sameAs": "https://www.facebook.com/kribzon/"
        }
    </script>
    <header class="bg-black-50">
        <div id="uppernav" class="uppernav">
            <div class="wrapper">
                <h1 class="d-inline text-center text-md-left">Hyundai | Kia | <span class="d-none d-sm-inline">Originalni</span> Rezervni Delovi</h1>
                <div class="upperright float-right">
                    <ul class="chooselang text-center float-left">
                        <li class="float-left upper_head_links d-none d-md-inline"><a href="">Gandijeva 152, Novi Beograd</a> |</li>
                        <li class="float-left upper_head_links d-none d-md-inline"><a href="tel:+381668828283">+381 66 882 82 83</a> |</li>
                        <li class="float-left currentlang">
                            <a href="">SRB</a>
                            <ul>
                                <li><a href="">ENG</a></li>
                                <li><a href="">KOR</a></li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="mainheader" id="mainheader">
            <div class="wrapper">
                <a href="index.php" class="logoholder">
                    <img src="images/hyundai-kia-rezervni-delovi-beograd-logo3.png" alt="" />
                </a>

                <nav id="mainnav" class="navbar-expand-lg">
                    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" id="maincolbutt">
                        <img src="images/hyundai-kia-rezervni-delovi-beograd-menu.svg" alt="Restaurant Manjez Belgrade" />
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav list-unstyled">
                            <li class="nav-item">
                                <a class="nav-link" href="">
                                    <h2>Početna</h2>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="">
                                    <h2>Hyundai delovi</h2>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="">
                                    <h2>Kia delovi</h2>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="">
                                    <h2>Veleprodaja</h2>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="">
                                    <h2>Kontakt</h2>
                                </a>
                            </li>
                            <li class="nav-item d-inline d-lg-none">
                                <div class="ctaheader2 d-flex justify-content-around mx-auto ">
                                    <div><a href="" data-remodal-target="modal4">
                                            <img src="images/hyundai-kia-rezervni-delovi-beograd-search.svg" alt="">
                                        </a></div>
                                    <div><a href="">
                                            <img src="images/hyundai-kia-rezervni-delovi-beograd-shopping-cart.svg" alt="">
                                        </a></div>
                                    <div><a href="#modal">
                                            <img src="images/hyundai-kia-rezervni-delovi-beograd-login.svg" alt="">
                                        </a></div>
                                    <div><a href="#modal">
                                            <img src="images/hyundai-kia-rezervni-delovi-beograd-phone.svg" alt="">
                                        </a></div>
                                    <div><a href="#modal">
                                            <img src="images/hyundai-kia-rezervni-delovi-beograd-location.svg" alt="">
                                        </a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="ctaheader d-none d-lg-flex row mx-0">
                    <div class="col-4 px-0"><a href="" data-remodal-target="modal4">
                            <img src="images/hyundai-kia-rezervni-delovi-beograd-search.svg" alt="">
                        </a></div>
                    <div class="col-4 px-0"><a href="">
                            <img src="images/hyundai-kia-rezervni-delovi-beograd-shopping-cart.svg" alt="">
                        </a></div>
                    <div class="col-4 px-0"><a href="#modal">
                            <img src="images/hyundai-kia-rezervni-delovi-beograd-login.svg" alt="">
                        </a></div>
                </div>
            </div>
        </div>
    </header>