 <!-- Search -->

 <section class="searchincluder bg-white text-center">
   <div class="wrapper py-5">
     <div class="search-info">
       <h4 class="display-4 maincol pb-5">Pretraga delova</h4>
     </div>

     <form action="" class="row">
       <div class="input form-group col-12 col-sm-6 mb-0">
         <select class="select-css sctdd maincol form-control my-3 h-auto py-3" name="" id="">
           <option value="" disabled selected>BREND AUTOMOBILA</option>
           <option value="">KIA</option>
           <option value="">Hyundai</option>
         </select>
       </div>
       <div class="input form-group col-12 col-sm-6 mb-0">
         <select class="select-css sctdd maincol form-control my-3 h-auto py-3" name="" id="">
           <option value="" disabled selected>MODEL AUTOMOBILA</option>
           <option value="">Lorem, ipsum</option>
           <option value="">Quasi, impedit</option>
           <option value="">Expedita, mollitia</option>
           <option value="">Commodi, quaerat</option>
           <option value="">Totam, vitae</option>
           <option value="">Eos, quisquam</option>
           <option value="">Sint, deleniti</option>
           <option value="">Dignissimos, ut</option>
           <option value="">Praesentium, accusamus</option>
           <option value="">Minus, quia</option>
         </select>
       </div>
       <div class="input form-group col-12 col-sm-6 mb-0">
         <select class="select-css sctdd maincol form-control my-3 h-auto py-3" name="" id="">
           <option value="" disabled selected>TIP</option>
           <option value="">Lorem</option>
           <option value="">Aliquam</option>
           <option value="">Eligendi</option>
           <option value="">Veniam</option>
           <option value="">Consequuntur</option>
           <option value="">Amet</option>
         </select>
       </div>
       <div class="input col-12 col-sm-6">
         <input class="form_deo form-control my-3 maincol" type="text" placeholder="DEO" />
       </div>
       <div class="input my-3 col-12 text-center mb-0">
         <input class="init_search_but form-control btn btn-lg btn-danger" type="submit" value="PRETRAGA" />
       </div>
     </form>
   </div>
 </section>