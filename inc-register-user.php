<section class="register-user-modal">
    <div class="wrapper">
        <div class="remodal rounded" data-remodal-id="modal3">
            <button data-remodal-action="close" class="remodal-close"></button>
            <h4 class="display-4 maincol mb-3">Registrujte se</h4>
            <p>Molimo popunite sledeće podatke da bi ste se registrovali:</p>
            <input class="form_deo form-control mt-3" type="Username" placeholder="E-mail">
            <input class="form_deo form-control mt-3" type="username" placeholder="Vaš username">
            <input class="form_deo form-control mt-3" type="password" placeholder="Lozinka">
            <div class="form-check mt-3">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label ml-1" for="exampleCheck1">Slažem se sa uslovima korišćenja</label>
            </div>
            <div class="form-check mt-3">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label ml-1" for="exampleCheck1">Garantujem da su unešeni podaci tačni</label>
            </div>

            <button class="btn btn-danger btn-lg mt-3">Registrujte se</button>

        </div>
    </div>
</section>